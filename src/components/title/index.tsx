import React from 'react'

interface ClassName {
  className?: string
}

export default function TitleComponent({ className }: ClassName): JSX.Element {
  return <h1 className={className}>.ONLY</h1>
}
