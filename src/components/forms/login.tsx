import React from 'react'
import { useForm, SubmitHandler } from 'react-hook-form'
import Auth from '../../lib/auth'
import { useHistory } from 'react-router-dom'
import { useState } from 'react'
import styled from 'styled-components'

const LoginForm = ({ className }: ClassName): JSX.Element => {
  const [errorMessage, setErrorMessage] = useState('')
  const [buttonDisabled, setButtonDisabled] = useState(false)
  const history = useHistory()
  const loginFunction = new Auth()
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>()
  const Input = styled.input`
    border: ${({ error }: InputInterface) =>
      error ? '1px solid #E26F6F' : 'none'};
  `
  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    setButtonDisabled(true)
    try {
      const response = await loginFunction.signIn(data)
      if (response.success) {
        history.push('/')
      } else setErrorMessage(response.message || 'Что-то пошло не так')
    } catch (error) {
      console.warn(error)
    }
    setButtonDisabled(false)
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={className}>
      {errorMessage.length > 1 ? (
        <div className="errMessage">
          <div className="errIcon" />
          <span>{errorMessage}</span>
        </div>
      ) : null}
      <div>
        <label htmlFor="login">Логин</label>
        <Input
          {...register('login', { required: true })}
          error={!!errors.login}
        />
        {errors.login && <span>Обязательное поле</span>}
      </div>
      <div>
        <label htmlFor="password">Пароль</label>
        <Input
          {...register('password', { required: true })}
          error={!!errors.password}
          type="password"
        />
        {errors.password && <span>Обязательное поле</span>}
      </div>
      <div className="passRemember">
        <Input type="checkbox" {...register('passRemember')} />
        <label htmlFor="passRemember">Запомнить пароль</label>
      </div>
      <div>
        <input type="submit" disabled={buttonDisabled} className="button" />
      </div>
    </form>
  )
}

export default LoginForm

interface Inputs {
  login: string
  password: string
  passRemember: boolean
}
interface ClassName {
  className?: string
}
interface InputInterface {
  error?: boolean
}
