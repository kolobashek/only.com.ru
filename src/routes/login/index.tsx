import React from 'react'
import Title from '../../components/title'
import LoginForm from '../../components/forms/login'

function Login(): JSX.Element {
  return (
    <main className="wrapper">
      <Title className="title" />
      <LoginForm className="body" />
    </main>
  )
}

export default Login
