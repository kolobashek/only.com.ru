import React from 'react'
import { useHistory } from 'react-router-dom'
import Cookies from 'universal-cookie'
import Title from '../../components/title'
import auth from '../../lib/auth'

const Auth = new auth()
const cookies = new Cookies()
function Profile(): JSX.Element {
  const history = useHistory()
  const logOut = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    Auth.logOut()
    history.push('/signin')
  }
  return (
    <main className="wrapper">
      <Title className="title" />
      <div className="profileContainer">
        <p>
          Здравствуйте, <b>{cookies.get('email')}</b>
        </p>
        <button onClick={logOut}>Выйти</button>
      </div>
    </main>
  )
}

export default Profile
