import Db from '../api/database'
import Cookies from 'universal-cookie'

interface Data {
  login: string
  password: string
}
interface Response {
  success: boolean
  email: string
  message?: string
}

const Database = new Db()
const cookies = new Cookies()

export default class auth {
  signIn = async (data: Data): Promise<Response> => {
    try {
      const response = await Database.signIn(data)
      cookies.set('email', response.email)
      return response
    } catch (error) {
      console.warn(error)
      return {
        success: false,
        email: '',
        message: 'Что-то пошло не так',
      }
    }
  }
  logOut = (): void => {
    cookies.remove('email')
  }
  isLogged = (): boolean => {
    if (cookies.getAll() && cookies.get('email')) return true
    return false
  }
}
