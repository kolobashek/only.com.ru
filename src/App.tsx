import React from 'react'
import { Route, BrowserRouter as Router } from 'react-router-dom'
import { Profile, Login } from './routes'

function App(): JSX.Element {
  return (
    <Router>
      <Route exact path="/">
        <Profile />
      </Route>
      <Route path="/signin">
        <Login />
      </Route>
    </Router>
  )
}

export default App
