interface Response {
  success: boolean
  email: string
  message: string
}
interface User {
  login: string
  password: string
}

export default class db {
  readonly users: User[] = [
    {
      login: 'steve.jobs@example.com',
      password: 'password',
    },
  ]

  signIn = (Data: User): Promise<Response> => {
    const user = this.users.find((item) => item.login === Data.login)
    const response = (message?: string) => {
      return {
        success: message ? false : true,
        email: user ? user.login : '',
        message: message || '',
      }
    }
    if (user) {
      if (user.password === Data.password) {
        return new Promise((resolve) => {
          setTimeout(() => resolve(response()), 1000)
        })
      }
      return new Promise((resolve) => {
        setTimeout(() => resolve(response('Неверный пароль')), 1000)
      })
    }
    return new Promise((resolve) => {
      setTimeout(
        () => resolve(response(`Пользователя ${Data.login} не существует`)),
        1000
      )
    })
  }
}
